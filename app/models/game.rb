class Game < ActiveRecord::Base
  attr_accessible :score
  attr_accessible :gamer_id
  
  validates_presence_of :gamer_id
  validates_presence_of :score
  
end
